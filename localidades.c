
/*
 * Copyright 2014 Spirok
 * Facundo Taramasco
 * Powered by GNU/Linux.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * comando construcción geany : gcc "%f" src/utilidades.c -I./include  -o "%e""
                                gcc -Wall ./src/utilidades.c -I ./include localidades.c -o localidades
 */

#include "utilidades.h"

/*Prototipos*/
void MenuApp();
void SectorAgregar();
void SectorEliminar();
void SectorModificar();
void SectorMostrarTodosLoc();
void SectorMostrarTodosCli();
void SectorRegenerarArchivo();


//############################### MAIN ########################################

int main( int argc, char **argv ) {
    if ( !AccionArchivo( PATHLOC, "rb" ) && !AccionArchivo( PATHCLI, "rb" ) ) {
        //Si no se encuentra el archivo se informa
         MsjError( "Error al leer archivo localidades-clientes. main()\n" );
         SectorRegenerarArchivo();
    }
    else MenuApp();
    return 0;
}

//#############################################################################


//############################### MENU ######################################## 

void MenuApp() {
    /*Función que muestra un menú y gestiona todas las
     opciones de la App.*/
    int opt;
    do {
        printf( "\t\t|| Sistema de ABM Localidades ||\n\n" );
        printf( "\t\t1) Agregar.\n" );
        printf( "\t\t2) Eliminar.\n" );
        printf( "\t\t3) Modificar.\n" );
        printf( "\t\t4) Listar todas las localidades.\n" );
        printf( "\t\t5) Listar todas los clientes.\n" );
        printf( "\t\t6) Regenerar Archivo Localidades.\n" );
        printf( "\t\t7) Salir.\n\n" );
        printf( "\t\tOpcion : " );
        scanf( "%d", &opt );
        getchar();
        system( CMDCLEAR );
        switch( opt ) {
            case 1:
                SectorAgregar();
                break;
            case 2:
                SectorEliminar();
                break;
            case 3:
                SectorModificar();
                break;
            case 4:
                SectorMostrarTodosLoc();
                break;
            case 5:
                SectorMostrarTodosCli();
                break;
            case 6:
                SectorRegenerarArchivo();
                break;
        }
    } while( opt != 7 );
}


//############################## ALTAS ######################################## 

void SectorAgregar() {
    /* Función encargada de dar alta una localidad.
    *Se realiza una validación sobre el campo 'codigo' para verificar
    que el mismo no existe en el archivo localidades.
    Se tiene en cuenta la posibilidad de agregar la localidad en un registro 
    donde estado == 0 (localidad eliminada de manera logica). Si no existe
    ningun registro estado == 0 se agrega al final. */
    int lugarlibre = 0;   // Al agregar busco si existe un lugar con estado == 0(loc. eliminado de forma logica)
    FILE *fileloc;
    FileStructLoc fs;    // almacena los datos de entrada por teclado
    FileStructLoc fsaux; // almacena las lecturas del archivo
    FileStructLoc fsret; // almacena datos del codigo existente
    printf( "Sector Agregar Localidad.\n\n" );
    IngresoString( fs.codigo, "Ingrese Codigo : " );
    IngresoString( fs.descripcion, "Ingrese descripción : " );
    fs.estado = 1; //Importante, setear a 1

    if ( ExistenciaCodLoc( fs.codigo ) ) {
        printf("\nLocalidad existente.");
        fsret = getLocalidad( fs.codigo );
        printf( "\nDescripción original : %s\n", fsret.descripcion );
        MsjContinuar();
        return;
    }

    if ( ( fileloc = fopen( PATHLOC, "rb+" ) ) == NULL ){
        MsjError( "Error al abrir archivo localidades. SectorAgregar() \n" );
        MsjContinuar();
        return;
    }

    while ( fread( &fsaux, sizeof(FileStructLoc), 1, fileloc ) == 1 ) {
        if ( fsaux.estado == 0 ) {
            fseek( fileloc, ftell(fileloc) - sizeof(FileStructLoc), SEEK_SET );
            fwrite( &fs, sizeof(FileStructLoc), 1, fileloc );
            lugarlibre = 1;
            break;
        }
    }
    //Si no existia ninguna localidad con campo estado 0 agrego al final
    if ( !lugarlibre )
        fwrite( &fs, sizeof(FileStructLoc), 1, fileloc );
    fclose( fileloc );
    printf( "\nLocalidad agregada correctamente." );
    MsjContinuar();
}



//############################## BAJAS ########################################

void SectorEliminar() {
    /* Funcion encargada de eliminar una localidad. Para eso se especifica su
    codigo. De no existir se informa, caso contrario se valida que NO existan
    clientes que dependan de la localidad a eliminar. Si esto sucede NO se 
    permite eliminar */
    FileStructLoc fs;    // almacena los datos de entrada por teclado
    FileStructLoc fsaux; // almacena las lecturas del archivo
    FileStructLoc fsret; // almacena datos antiguos del codigo a eliminar
    FILE *fileloc;
    printf( "Sector Eliminar Localidad.\n\n" );
    IngresoString( fs.codigo, "Ingrese Codigo : " );

    //-------------------------------------------------------- NUEVA FORMA

    if ( !ExistenciaCodLoc( fs.codigo ) ) {
        printf( "Localidad inexistente.\n" );
        MsjContinuar();
        return;
    }

    fsret = getLocalidad( fs.codigo );
    printf( "Descripción de Localidad : %s\n", fsret.descripcion );

    if ( ClienteTieneLocalidad( fs.codigo ) ){ 
        printf( "Existen clientes que dependen de esta localidad\n" );
        MsjContinuar();
        return;
    }

    if ( !MsjSiNo( "\n¿Desea borrar la localidad? (s/n) : " ) ) {
        MsjContinuar();
        return;
    }

    if ( ( fileloc = fopen( PATHLOC, "rb+" ) ) == NULL ) {
        MsjError( "Error al abrir archivo localidades. SectorEliminar()\n" );
        MsjContinuar();
        return;
    }

    // Si elijio que quiere eliminar la localidad y no hay error en el archivo.
    // IMPORTANTISIMO setear a 0, ya que con esto se realiza la baja logica.
    fs.estado = 0;
    while ( fread( &fsaux, sizeof(FileStructLoc), 1, fileloc ) == 1 ) {
        if ( strcmp( fs.codigo, fsaux.codigo ) == 0 ) { //Comparo codigos, si lo encontre.
            // vuelvo al registro que eleji eliminar.
            fseek( fileloc, ftell(fileloc) - sizeof(FileStructLoc), SEEK_SET );
            // tomo su descripcion, esto realmente es innecesario.
            strcpy( fs.descripcion, fsaux.descripcion );
            // y escribo los datos, lo que en realidad cambia es estado 1 a estado 0
            fwrite( &fs, sizeof(FileStructLoc), 1, fileloc );
            break;
        }
    }
    fclose( fileloc );
    printf( "\nLocalidad eliminada correctamente." );
    MsjContinuar();
}

    /* VIEJA FORMA
    if (!ExistenciaCodLoc(fs.codigo)){
        printf("Localidad inexistente.\n");
        MsjContinuar();
    }//Si existe el codigo pregunto si existen clientes que dependan de la
    //localidad ingresada.
    else if (ClienteTieneLocalidad(fs.codigo) ){ 
        printf("Existen clientes que dependen de esta localidad\n");
        MsjContinuar();
    }else{//Si no hay errores
        //Muestro la descripcion de dicha localidad.
        fsret = getLocalidad(fs.codigo);
        printf("Descripción de Localidad : %s\n",fsret.descripcion);
        //Pregunto si quiere eliminar la localidad. 1 si 0 no
        if (MsjSiNo("\n¿Desea borrar la localidad? (s/n) : ")){
            //Si elijio que la quiere eliminar.
            if ((fileloc=fopen(PATHLOC,"rb+")) == NULL){
                MsjError("Error al abrir archivo localidades. SectorEliminar()\n");
            }else{
                //Si elijio que quiere eliminar la localidad y no hay error en el archivo.
                //IMPORTANTISIMO setear a 0, ya que con esto se realiza la baja logica.
                fs.estado=0;
                //Leo el archivo en busca del codigo ingresado.
                while(fread(&fsaux,sizeof(FileStructLoc),1,fileloc) == 1){
                    if (strcmp(fs.codigo,fsaux.codigo) == 0){ //Comparo codigos, si lo encontre.
                        //Vuelvo al registro que eleji eliminar.
                        fseek(fileloc,ftell(fileloc)-sizeof(FileStructLoc),SEEK_SET);
                        //Tomo su descripcion, esto realmente es innecesario.
                        strcpy(fs.descripcion,fsaux.descripcion);
                        //Y escribo los datos, lo que en realidad cambia es estado 1 a estado 0
                        fwrite(&fs,sizeof(FileStructLoc),1,fileloc);
                        break;
                    } //end if strcmp == 0
                } //end while
                fclose(fileloc);
                printf("\nLocalidad eliminada correctamente.");
                MsjContinuar();
            } //end else fopen ok
        } //end if MsjSino
    } //end else dependencias ok
    system(CMDCLEAR);
}
*/

//########################## MODIFICACIONES ###################################

void SectorModificar() {
    /* Función encargada de modificar una localidad (solo se puede
     modificar su descripción). Para eso es necesario ingresar un codigo,
     el cual se valida si existe en el archivo. Llegado el caso que el codigo
     no exista se informa, caso contrario se actualiza descripcion.*/
    FILE *fileloc;
    FileStructLoc fs;    // almacena los datos de entrada por teclado
    FileStructLoc fsaux; // almacena las lecturas del archivo
    FileStructLoc fsret; // almacena datos antiguos del codigo a modificar
    printf( "Sector Modificar Localidad.\n\n" );
    //Codigo de la descripción que desea modificar.
    IngresoString (fs.codigo, "Ingrese Codigo : ");

    // --------------------------------------------------------
    if (!ExistenciaCodLoc(fs.codigo)){
        printf("\nCodigo Inexistente.");
        MsjContinuar();
        return;
    }

    fs.estado = 1; //Importante, setear a 1
    fsret = getLocalidad( fs.codigo );
    printf( "\nDescripción original : %s\n", fsret.descripcion );
    //Nueva descripcion ingresada.
    IngresoString( fs.descripcion, "\nNueva Descripción : " );

    if ( ( fileloc = fopen( PATHLOC, "rb+" ) ) == NULL ) {
        MsjError( "Error al abrir archivo localidades. SectorModificar()\n" );
        MsjContinuar();
        return;
    }

    while ( fread( &fsaux, sizeof(FileStructLoc), 1, fileloc ) == 1 ) {
        //Si encontre el codigo de la descripción que modificó.
        if ( strcmp( fs.codigo, fsaux.codigo ) == 0 ) {
            //Me posiciono en la localidad anterior (ya que leí y el cursor bajo 1 pos.)
            fseek( fileloc, ftell(fileloc) - sizeof(FileStructLoc), SEEK_SET );
            //Y Actualizo los datos.
            fwrite( &fs, sizeof(FileStructLoc), 1, fileloc );
            break;
        }
    }
    fclose( fileloc );
    printf( "\nLocalidad modificada correctamente." );
    MsjContinuar();
}


/*
    //Si el codigo ingresado no existe en el archivo.
    if (!ExistenciaCodLoc(fs.codigo)){
        printf("\nCodigo Inexistente.");
        MsjContinuar();
    }else{
        fs.estado=1; //Importante, setear a 1
        //Si el codigo existe tomo su descripcion y la muestro.
        fsret = getLocalidad(fs.codigo);
        printf("\nDescripción original : %s\n", fsret.descripcion );
        //Nueva descripcion ingresada.
        IngresoString(fs.descripcion, "\nNueva Descripción : ");
        //Intento abrir el archivo modo lectura y escritura.
        if ((fileloc=fopen(PATHLOC,"rb+")) == NULL){
            MsjError("Error al abrir archivo localidades. SectorModificar()\n");
        }//Si no hubo errores al abrir archivo.
        else{
            //Mientras no llegue a fin de archivo y no encuntre la localidad a modificar
            while(fread(&fsaux, sizeof(FileStructLoc), 1, fileloc) == 1){
                //Si encontre el codigo de la descripción que modificó.
                if (strcmp(fs.codigo, fsaux.codigo) == 0){
                    //Me posiciono en la localidad anterior (ya que leí y el cursor bajo 1 pos.)
                    fseek(fileloc, ftell(fileloc)-sizeof(FileStructLoc), SEEK_SET);
                    //Y Actualizo los datos.
                    fwrite(&fs, sizeof(FileStructLoc), 1, fileloc);
                    //Ya modifique entonces salgo.
                    break;
                } //end if strcmp == 0
            } //end while
            fclose(fileloc);
            printf("\nLocalidad modificada correctamente.");
            MsjContinuar();
        } //end else fopen ok
    } //end else codigo ok
    system(CMDCLEAR);
}
*/

//########################## LISTADOS COMPLETOS ###############################

/*Listado completo de datos de todos los clientes.*/
void SectorMostrarTodosCli() {
    ListadoClientes();
    MsjContinuar();
}

/*Listado completo de datos de todos las localidades.*/
void SectorMostrarTodosLoc() {
    ListadoLocalidades();
    MsjContinuar();
}

//########################## REGENERAR ARCHIVO ################################

void SectorRegenerarArchivo() {
    //Mensaje informativo. Puede elejir si o no
    if ( MsjSiNo( "¡¡¡ATENCION!!!.\nAl regenerar el archivo localidades \
        \ndeberá regenerar el archivo clientes.\n¿Desea continuar?(s/n) : " ) ) {
        //Si elijio si regenero ambos archivos.
       if ( AccionArchivo (PATHLOC, "wb" ) && AccionArchivo( PATHCLI, "wb") ) {
        printf( "Archivos regenerados correctamente.\n" );
        }
        else MsjError( "Error al regenerar archivos localidades y clientes. SectorRegenerarArchivo()\n" );
    }
    MsjContinuar();
}
