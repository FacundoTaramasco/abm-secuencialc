
/*
 * Copyright 2014 Spirok
 * Facundo Taramasco
 * Powered by GNU/Linux.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * comando construcción geany : gcc "%f" src/utilidades.c -I./include  -o "%e"
                                gcc -Wall ./src/utilidades.c -I ./include consulta.c -o consulta
 */

#include "utilidades.h"

void MenuApp();
void SectorConsultaCli();
void SectorMostrarTodosLoc();
void SectorMostrarTodosCli();

//############################### MAIN ########################################

int main( int argc, char **argv ) {
    if ( !AccionArchivo( PATHLOC, "rb" ) && !AccionArchivo( PATHCLI, "rb") ) {
        //Si no se encuentra el archivo se informa y se regenera
         MsjError( "Error al leer archivo localidades-clientes. main()\n" );
    }
    else MenuApp();
    return 0;
}

//#############################################################################


//############################### MENU ######################################## 

void MenuApp() {
    /*Función que muestra un menú y gestiona todas las
     opciones de la App.*/
    int opt;
    do {
        printf( "\t\t|| Sistema de Consulta Localidades/Clientes ||\n\n" );
        printf( "\t\t1) Consulta Cliente.\n" );
        printf( "\t\t2) Listar todos las clientes.\n ");
        printf( "\t\t3) Listras todas las localidades.\n" );
        printf( "\t\t4) Salir.\n\n" );
        printf( "\t\tOpcion : " );
        scanf( "%d", &opt );
        system( CMDCLEAR );
        getchar();
        switch( opt ) {
            case 1:
                SectorConsultaCli();
                break;
            case 2:
                SectorMostrarTodosCli();
                break;
            case 3:
                SectorMostrarTodosLoc();
                break;
        }
    } while( opt != 4 );
}


//######################### CONSULTA CLIENTE ##################################

void SectorConsultaCli() {
    /*Funcion encargada de realizar una consulta de cliente (desde el archivo
     cliente). Para ello se especifica su codigo y en caso de existir se 
     muestran sus datos (codigo,nombre,codigo localidad,descripcion localidad)
     Esta ultima se obtiene desde el arhivo localidades. */
    FileStructCli fs;
    FileStructCli fsauxcli;
    FileStructLoc fsauxloc;
    printf("Sector Consulta Cliente.\n\n");
    IngresoString(fs.codigo,"Ingrese Codigo Cliente : ");

    if ( !ExistenciaCodCli( fs.codigo ) ) {
        printf( "Cliente inexistente.\n" );
        MsjContinuar();
        return;
    }

    fsauxcli = getCliente( fs.codigo );
    fsauxloc = getLocalidad( fsauxcli.codigo_loc );
    printf( "\n%-20s %-20s %-10s\n", "Cod. Cliente", "Nombre", "Cod. Loc.");
    printf( "----------------------------------------------------------\n");
    printf( "%-20s %-20s %s (%s)\n", fsauxcli.codigo, fsauxcli.nombre,
    fsauxcli.codigo_loc, fsauxloc.descripcion );
    MsjContinuar();
}

//########################## LISTADOS COMPLETOS ###############################

void SectorMostrarTodosCli() {
    ListadoClientes();
    MsjContinuar();
}

void SectorMostrarTodosLoc() {
    ListadoLocalidades();
    MsjContinuar();
}
