
/*
 * Copyright 2014 Spirok
 * Facundo Taramasco
 * Licencia GPL.( Es libre troesma, hace lo que se te cante).
 * Un ABM de la concha de la madre, codeado de la forma mas ineficiente posible
 * (archivos, encima secuenciales...). Si encontras bug los arreglas, sino jodete.
 * Powered by GNU/Linux.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
 
#include "utilidades.h"

//########################### DATA ENTRY ######################################

 /*Funcion encargada de la data-entry de string.
  Recibe:
   char* s(donde se almacenará el string a ingresar)
   char* msj (mensaje a mostrar)*/
void IngresoString( char* s, char* msj ) {
    printf( "%s",msj );
    scanf( " %[^\n]s", s );
    getchar();
}
//#############################################################################




//########################### VALIDACIONES ####################################

/**string.h --> strcmp(char* s1,char* s2):
 Devuelve 0 si las cadenas de texto son iguales (incluyendo mayúsculas
 y minúsculas); si la primera cadena es mayor que la segunda, devuelve
 un número positivo; si es mayor la segunda, devuelve un valor negativo*/


/*Función que recibe un codigo y verifica si existe en el archivo
  localidades (no se tienen en cuenta localidades con estado == 0).
  Recibe : codigo localidad.
  Retorna :
   0 Si no existe.
   1 Si existe.*/
int ExistenciaCodLoc( char *codcmp ) {
    FILE *fileloc;
    FileStructLoc fs;
    int existe = 0;
    if ( (fileloc = fopen( PATHLOC, "rb" ) ) == NULL ) {
        MsjError( "Error al abrir archivo localidades. ExistenciaCodLoc()\n" );
    } else {
        // Mientras no sea fin de archivo y no encuentra el codigo.
        while ( fread( &fs, sizeof(FileStructLoc), 1, fileloc) == 1 ) {
            if ( fs.estado == 1 ) { //Si es una localidad que existe
                if ( strcmp( fs.codigo, codcmp ) == 0 ) { //si el codigo ingresado es igual al del archivo.
                    existe = 1;
                    break;
                }
            }
        }
        fclose( fileloc );
    }
    if ( existe ) return 1;
    else return 0;
}


/*Función que recibe un codigo y verifica si existe en el archivo
  localidades (no se tienen en cuenta clientes con estado == 0).
  Recibe : codigo de cliente.
  Retorna :
  0 Si no existe.
  1 Si existe.*/
int ExistenciaCodCli( char *codcmp ) {
    FILE *filecli;
    FileStructCli fs;
    int existe = 0; 
    if ( (filecli = fopen( PATHCLI,"rb" ) ) == NULL ) {
        MsjError( "Error al abrir archivo clientes. ExistenciaCodCli()\n" );
    } else {
        // Mientras no sea fin de archivo y no encuentra el codigo.
        while( fread(&fs, sizeof(FileStructCli), 1, filecli) ==1 ) {
            if ( fs.estado == 1 ) { // Si es una localidad que existe
                // si el codigo ingresado es igual al del archivo.
                if ( strcmp( fs.codigo, codcmp ) == 0) { 
                    existe = 1;
                    break;
                }
            }
        }
        fclose( filecli );
    }
    if ( existe ) return 1;
    else return 0;
}

/*Funcion que verifica si existe al menos un cliente que depende de una 
  localidad ("codloc").
  Esta función es usada para validar si una localidad puede ser borrada.
  Recibe : codigo de localidad.
  Retorna : 
   Si no existen depedencias retorna 1.
   Si existen dependencias retorna 0.*/
int ClienteTieneLocalidad( char* codloc ) {
    FILE *filecli;
    FileStructCli fs;
    int existe = 0;
    if ( ( filecli = fopen( PATHCLI, "rb" ) ) == NULL) {
        MsjError( "Error al abrir archivo clientes. ClienteTieneLocalidad()\n" );
    } else {
        while ( fread(&fs, sizeof(FileStructCli), 1, filecli) == 1 ) {
            if ( strcmp( fs.codigo_loc, codloc ) == 0 ) {
               existe = 1;
               break; 
            }
        }
        fclose(filecli);
    } 
    if ( existe ) return 1;
    else return 0;
}

//#############################################################################


/*Función que recibe un codigo de localidad, lee desde el archivo localida-
  des en busca de ese codigo y retorna una estructura con todos los datos
  del mismo(codigo, descripcion, estado).
  ANTES DE LLAMAR A ESTA FUNCION VERIFICAR QUE LA LOCALIDAD EXISTA EN EL ARCHIVO
  Recibe : codigo de localidad.
  Retorna :
   Una estructura con toda la info. de esa localidad.*/
FileStructLoc getLocalidad( char* cod ) {
    FILE *fileloc;
    FileStructLoc fs;
    //Intento abrir el archivo.
   if ( ( fileloc = fopen( PATHLOC, "rb" ) ) == NULL ) {
       MsjError( "Error al abrir archivo localidades. getLocalidad()\n" );
    } else {
        // Leo el archivo buscando el codigo.
        while( fread(&fs, sizeof(FileStructLoc), 1, fileloc) == 1 ) {
            if ( fs.estado == 1 ) { // Si es una localidad que existe
                if ( strcmp(fs.codigo, cod) == 0 ) { // Comparo los codigos.
                    break;
                }
            }
        }
        fclose( fileloc );
    }
    return fs;
}

/*Función que recibe un codigo de cliente, lee desde el archivo clientes
  en busca de ese codigo y retorna una estructura con todos los datos
  del mismo(codigo,nombre,estado,codigo_localidades).
  ANTES DE LLAMAR A ESTA FUNCION VERIFICAR QUE EL CLIENTE EXISTA EN EL ARCHIVO
  Recibe : codigo de cliente.
  Retorna :
   Una estructura con toda la info. de ese cliente.*/
FileStructCli getCliente( char* cod ) {
    FILE *filecli;
    FileStructCli fs;
    // Intento abrir el archivo.
   if ( ( filecli = fopen( PATHCLI, "rb") ) == NULL ){
       MsjError( "Error al abrir archivo clientes. getCliente()\n" );
    } else {
        // Leo el archivo buscando el codigo.
        while( fread(&fs, sizeof(FileStructCli), 1, filecli) == 1 ) {
            if ( fs.estado == 1 ) { // Si es una localidad que existe
                if ( strcmp( fs.codigo, cod ) == 0 ) { // Comparo los codigos.
                    break;
                }
            }
        }
        fclose(filecli);
    }
    return fs;
}


/*Función encargada de mostrar un listado completo de localidades.
  Usada tanto en App localidades como en clientes.*/
void ListadoLocalidades() {
    FILE *fileloc;
    FileStructLoc fs;
    // Intento abrir el archivo localidades
    if ( ( fileloc = fopen( PATHLOC, "rb" ) ) == NULL ) {
       MsjError( "Error al abrir archivo localidades. ListadoLocalidades()\n" );
    } else {
        printf( "Sector Listado Localidades :\n\n" );
        printf( "%-10s %-20s %-10s\n", "Codigo", "Descripcion", "Estado" );
        printf( "----------------------------------------------\n" );
        // Mientras no llegue a fin de linea leo y muestro el contenido.
        while( fread( &fs, sizeof(FileStructLoc), 1, fileloc) == 1 ) {
            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Filtrar por estado == 1 aca !!
            printf( "%-10s %-20s %-10d\n", fs.codigo, fs.descripcion, fs.estado );
        }
        fclose( fileloc );
    }
}


/*Función encargada de mostrar un listado completo de clientes.
  Usada tanto en App localidades como en clientes.*/
void ListadoClientes() {
    FILE *filecli;
    FileStructCli fs;
    FileStructLoc fsl; // almacena datos actuales del cliente con respecto a su localidad.
    // Intento abrir el archivo localidades
    if ( ( filecli = fopen( PATHCLI, "rb" ) ) == NULL ) {
        MsjError( "Error al abrir archivo clientes. ListadoClientes()\n" );
    } else {
        printf( "Sector Listado Clientes :\n\n" );
        printf( "%-10s %-20s %-10s %-10s\n", "Codigo", "Nombre", "Estado", "Cod. Loc." );
        printf( "----------------------------------------------------------\n" );
        // Mientras no llegue a fin de linea leo y muestro el contenido.
        while( fread(&fs, sizeof(FileStructCli), 1, filecli) == 1 ) {
            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Filtrar por estado == 1 aca !!
            fsl = getLocalidad( fs.codigo_loc );
            printf( "%-10s %-20s %-10d %s (%s)\n", 
            fs.codigo, fs.nombre, fs.estado, fs.codigo_loc, fsl.descripcion);
        }
        fclose( filecli );
    } // end else
}



/*Función que verifica si se puede abrir el archivo segun sea mode.
  Recibe : 
   char* path (ruta de archivo)
   char* mode (que representa "w" o "r" )
 Retorna :
  1 si esta ok.
  0 si existe error. */
int AccionArchivo( char* path,char* mode ){
    FILE *fileloc;
    if ( ( fileloc = fopen( path, mode ) ) == NULL ) {
        return 0;
    } else {
        fclose( fileloc );
        return 1;
    }
}


void MsjError( char* msj ) {
    printf( "%s", msj );
}
void MsjContinuar() {
    printf( "\nPresione Enter para continuar..." );
    getchar();
    system( CMDCLEAR );
}

int MsjSiNo( char* msj ) {
    char respuesta;
    printf( "%s", msj );
    scanf( "%c", &respuesta );
    getchar();
    if ( respuesta == 's' || respuesta == 'S' ) return 1;
    else return 0;
}
