
/*
 * Copyright 2014 Spirok
 * Facundo Taramasco
 * Powered by GNU/Linux.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * comando construcción geany : gcc "%f" src/utilidades.c -I./include  -o "%e"
                                gcc -Wall ./src/utilidades.c -I ./include clientes.c -o clientes
 */

#include "utilidades.h"

/*Prototipos*/
void MenuApp();
void SectorAgregar();
void SectorEliminar();
void SectorModificar();
void SectorMostrarTodosLoc();
void SectorMostrarTodosCli();
void SectorRegenerarArchivo();

//############################### MAIN ########################################

int main(int argc, char **argv) {
    if ( !AccionArchivo(PATHLOC,"rb")  && !AccionArchivo(PATHCLI,"rb") ) {
        //Si no se pudieron leer los archivos clientes y localidades
         MsjError( "Error al leer archivo localidades-clientes. main()\n" );
         SectorRegenerarArchivo();
    }
    else MenuApp();
    return 0;
}

//#############################################################################


//############################### MENU ######################################## 

void MenuApp() {
    /*Función que muestra un menú y gestiona todas las
     opciones de la App.*/
    int opt;
    do {
        printf( "\t\t|| Sistema de ABM Clientes ||\n\n" );
        printf( "\t\t1) Agregar.\n" );
        printf( "\t\t2) Eliminar.\n" );
        printf( "\t\t3) Modificar.\n" );
        printf( "\t\t4) Listar todos las clientes.\n" );
        printf( "\t\t5) Listras todas las localidades.\n" );
        printf( "\t\t6) Regenerar Archivo clientes.\n" );
        printf( "\t\t7) Salir.\n\n" );
        printf( "\t\tOpcion : ");
        scanf( "%d", &opt );
        system( CMDCLEAR );
        getchar();
        switch( opt ) {
            case 1:
                SectorAgregar();
                break;
            case 2:
                SectorEliminar();
                break;
            case 3:
                SectorModificar();
                break;
            case 4:
                SectorMostrarTodosCli();
                break;
            case 5:
                SectorMostrarTodosLoc();
                break;
            case 6:
                SectorRegenerarArchivo();
                break;
        }
    } while( opt != 7 );
}


//############################## ALTAS ######################################## 

/*Funcion encargada de agregar un cliente. Para ello se debe ingresar :
  codigo, nombre, codigo_localidad.
  Se realizan las siguiente validaciones : 
  -El codigo de cliente ingresado NO DEBE existir en el archivo clientes.
  -El codigo de localidad DEBE existir en el archivo localidades.
  Recibe  : nada.
  Retorna : nada.*/
void SectorAgregar() {
    FILE *filecli;
    int lugarlibre = 0;
    FileStructCli fs;    // Almacena datos de entrada por teclado.
    FileStructCli fsaux; // Almacena datos leidos del archivo clientes.
    FileStructCli fsret; // almacena datos actuales del cliente en caso que exista.
    FileStructLoc fsl;   // almacena datos actuales del cliente con respecto a su localidad.
    printf( "Sector Agregar Cliente\n\n" );
    IngresoString( fs.codigo, "Ingrese Codigo : " );
    IngresoString( fs.nombre, "Ingrese Nombre : " );
    IngresoString( fs.codigo_loc, "Ingrese Codigo Localidad : " );
    fs.estado = 1; // Importante, setear a 1
    if ( ExistenciaCodCli(fs.codigo) ) {
        printf( "\nCliente existente.\n" );
        fsret = getCliente( fs.codigo );
        fsl = getLocalidad( fsret.codigo_loc );
        printf( "\nNombre original : %s\n", fsret.nombre );
        printf( "Codigo Localidad original : %s (%s) \n", fsret.codigo_loc, fsl.descripcion );
        MsjContinuar();
        return;
    }
    if ( !ExistenciaCodLoc(fs.codigo_loc) ){
        printf("\nCodigo localidad inexistente.\n");
        MsjContinuar();
        return;
    }
    if ( (filecli = fopen(PATHCLI,"rb+")) == NULL ) {
        MsjError( "Error al abrir archivo clientes. SectorAgregar()\n" );
        MsjContinuar();
        return;
    }
    while( (fread(&fsaux, sizeof(FileStructCli), 1, filecli)) == 1  ) {
        // Busco un cliente con estado == 0 (cliente que anteriormente fue dado de baja de forma logica)
        if ( fsaux.estado == 0 ) {
            // Me posiciono en el cliente anterior (ya que leí y el cursor bajo 1 pos.)
            fseek( filecli, ftell(filecli) - sizeof(FileStructCli), SEEK_SET );
            // Agrego el cliente
            fwrite( &fs, sizeof(FileStructCli), 1, filecli );
            lugarlibre = 1;
            break;
        }
    }
    // Si no existia ningun cliente con estado == 0 agrego al final.
    if ( !lugarlibre )
        fwrite( &fs, sizeof(FileStructCli), 1, filecli );
    fclose( filecli );
    printf( "\nCliente agregado correctamente." );
    MsjContinuar();
}


//############################## BAJAS ########################################

/*Funcion encargada de eliminar una localidad. Para eso se especifica su
  codigo. De no existir se informa, caso contrario se valida que NO existan
  clientes que dependan de la localidad a eliminar. Si esto sucede NO se 
  permite eliminar, sino no.
  Recibe  : nada.
  Retorna : nada.*/
void SectorEliminar() {
    FileStructCli fs;    // almacena los datos de entrada por teclado
    FileStructCli fsaux; // almacena las lecturas del archivo
    FileStructCli fsret; // almacena datos actuales del cliente a eliminar
    FileStructLoc fsl;   // almacena datos actuales del cliente con respecto a su localidad.
    FILE *filecli;
    printf("Sector Eliminar Cliente.\n\n");
    IngresoString(fs.codigo, "Ingrese Codigo : ");

    if ( !ExistenciaCodCli(fs.codigo) ){
        printf( "Cliente inexistente.\n" );
        MsjContinuar();
        return;
    }

    // Muestro datos actuales del cliente
    fsret = getCliente( fs.codigo );
    fsl   = getLocalidad( fsret.codigo_loc );
    printf("\nNombre de Cliente : %s\n", fsret.nombre);
    printf("Codigo Localidad  : %s (%s) \n", fsret.codigo_loc, fsl.descripcion );

    if ( !MsjSiNo("\n¿Desea borrar el cliente? (s/n) : ") ) {
        MsjContinuar();
        return;
    }

    if ((filecli=fopen(PATHCLI, "rb+")) == NULL) {
        MsjError("Error al abrir archivo clientes. SectorEliminar()\n");
        MsjContinuar();
        return;
    }

    // IMPORTANTISIMO setear a 0, ya que con esto se realiza la baja logica.
    fs.estado=0;
    // Leo el archivo en busca del codigo ingresado.
    while( fread( &fsaux, sizeof(FileStructCli), 1, filecli ) == 1){
        if ( strcmp( fs.codigo, fsaux.codigo ) == 0 ) { //Comparo codigos, si lo encontre.
            // Vuelvo al registro que eleji eliminar.
            fseek( filecli, ftell(filecli)-sizeof(FileStructCli), SEEK_SET );
            // Tomo su nombre y cod. loc. , esto realmente es innecesario.
            strcpy( fs.nombre, fsaux.nombre );
            strcpy( fs.codigo_loc, fsaux.codigo_loc );
            //Y escribo los datos, lo que en realidad cambia es estado 1 a estado 0
            fwrite( &fs, sizeof(FileStructCli), 1, filecli );
            break;
        }
    }
    fclose(filecli);
    printf("\nCliente eliminado correctamente.");
    MsjContinuar();
}


//########################## MODIFICACIONES ###################################

/*Funcion encargada de modificar un cliente. Para ello se debe ingresar el
  codigo del mismo.
  Se realizan las siguiente validaciones : 
  -El codigo de cliente ingresado a modifircarse DEBE existir en 
   el archivo clientes.
  -El codigo de localidad ingresado a modificarse DEBE existir en
   el archivo localidades.
   Recibe  : nada.
   Retorna : nada.*/
void SectorModificar() {
    FILE *filecli;
    FileStructCli fs;    // almacena datos de entrada por teclado.
    FileStructCli fsaux; // almacena datos leidos del archivo.
    FileStructCli fsret; // almacena datos actuales del cliente a modificar
    FileStructLoc fsl;   // almacena datos actuales del cliente con respecto a su localidad.
    printf( "Sector Modificar Cliente\n\n" );
    IngresoString( fs.codigo, "Ingrese Codigo : " );

    if ( !ExistenciaCodCli( fs.codigo ) ) {
        printf("Cliente inexistente.\n");
        MsjContinuar();
        return;
    }

    // Muestro datos actuales del cliente
    fsret = getCliente( fs.codigo );
    fsl   = getLocalidad( fsret.codigo_loc );
    printf( "\nNombre original : %s\n", fsret.nombre );
    printf( "Codigo Localidad original : %s (%s) \n", fsret.codigo_loc, fsl.descripcion );
    IngresoString( fs.nombre, "\nIngrese nuevo Nombre : " );
    IngresoString( fs.codigo_loc, "Ingrese nuevo Cod. de Loc. : " );
    fs.estado = 1;

    if ( !ExistenciaCodLoc( fs.codigo_loc) ) { // Si la nueva localidad ingresada no existe
        printf("\nCodigo localidad inexistente.\n");
        MsjContinuar();
        return;
    } 

    if ( ( filecli = fopen( PATHCLI, "rb+" ) ) == NULL ) {
        MsjError( "Error al abrir archivo clientes. SectorEliminar()\n" );
         MsjContinuar();
        return;
    }

    // Leo el archivo en busca del cliente a modificar.
    while( fread( &fsaux, sizeof(FileStructCli), 1, filecli ) == 1 ) {
        if ( strcmp( fs.codigo, fsaux.codigo ) == 0 ) { //Comparo codigos, si lo encontre.
            // Vuelvo al registro que eleji modificar.
            fseek( filecli, ftell(filecli) - sizeof(FileStructCli), SEEK_SET );
            fwrite( &fs, sizeof(FileStructCli), 1, filecli );
            break;
        }
    }
    fclose( filecli );
    printf( "\nCliente modificado correctamente." );
    MsjContinuar();
}



//####################### LISTADOS COMPLETOS ##################################

/*Listado completo de datos de todos los clientes.*/
void SectorMostrarTodosCli() {
    ListadoClientes();
    MsjContinuar();
}

/*Listado completo de datos de todos las localidades.*/
void SectorMostrarTodosLoc() {
    ListadoLocalidades();
    MsjContinuar();
}

//########################## REGENERAR ARCHIVO ################################

/*Se regenera el archivo clientes nuevamente(los datos previos se pierden si es que existia).*/
void SectorRegenerarArchivo() {
    // Mensaje informativo. Puede elejir si o no
    if ( MsjSiNo("¡¡¡ATENCION!!!.\nRegenerar el archivo clientes implica perder\
        \ndatos previos de los mismos si es que existían.\n¿Desea continuar?(s/n) : ") ) {
        if ( AccionArchivo( PATHCLI,"wb" ) )
            printf( "Archivo regenerado correctamente.\n" );
        else
            MsjError( "Error al escribir archivo clientes. SectorRegenerarArchivo().\n" );
    }
    MsjContinuar();
}
