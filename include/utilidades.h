
/*
 * Copyright 2014 Spirok
 * Facundo Taramasco
 * Licencia GPL.( Es libre troesma, hace lo que se te cante).
 * Un ABM de la concha de la madre, codeado de la forma mas ineficiente posible
 * (archivos, encima secuenciales...). Si encontras bug los arreglas, sino jodete.
 * Powered by GNU/Linux.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef UTILIDADES
#define UTILIDADES

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*Comando para limpiar la pantalla.
    "cls"   : window
    "clear" : linux */
#define CMDCLEAR "clear"

// Ruta del archivo localidades.
#define PATHLOC "./Data/localidades.dat"
// Ruta del archivo clientes.
#define PATHCLI "./Data/clientes.dat"

#define LENDESC 30
#define LENCOD 15

// Estructura usada en el archivo localidades.
typedef struct {
    char codigo[LENCOD];
    char descripcion[LENDESC];
    unsigned char estado;    // Marca para determinar si existe la localidad (1 si, 0 no)
}FileStructLoc;


// Estructura usada en el archivo clientes.
typedef struct {
    char codigo[LENCOD];
    char nombre[LENDESC];
    unsigned char estado;    // Marca para determinar si existe el cliente (1 si, 0 no)
    char codigo_loc[LENCOD]; // Codigo que referencia una localidad
}FileStructCli;


int AccionArchivo( char* path,char* mode );

void IngresoString( char* s, char* msj );

int ExistenciaCodLoc( char* codcmp );
int ExistenciaCodCli( char* codcmp );
int ClienteTieneLocalidad( char* codloc );

FileStructLoc getLocalidad( char* cod );
FileStructCli getCliente( char* cod );

void ListadoLocalidades();
void ListadoClientes();

void MsjError();
void MsjContinuar();
int MsjSiNo( char* msj );

#endif
